var app = new Vue ({
    el:"#Citrus",
    data:{
        products:[{id:1,title:"Unshiu", short_text:'The main advantage of this citrus is its resistance to low temperatures.', image:"Unshiu.jpg",desc:"Full desc"},
        {id:2,title:"Austere", short_text:"A group of varieties of tangerines of Chinese origin.", image:"austere.jpg",desc:"full desc"},
        {id:3,title:"Deliciosa", short_text:"Group, whose members have a similar morphology to the Chinese group.", image:"deliciosa.jpg",desc:"full desc"},
        {id:4,title:"Reticulata", short_text:"Group of mandarins, which are of great industrial in China and India.", image:"reticulata.jpg",desc:"full desc"},
        {id:5,title:"Nobilis", short_text:"A distinctive feature of the varieties are large fruits, thick lumpy rind.", image:"nobilis.jpg",desc:"full desc"}],
        product: [{}],
            tovar: [],
            contactFields: [
                { caption: 'Name', text:''},
                { caption: 'Company Name', text:''},
                { caption: 'Position', text:'' },
                { caption: 'City', text:''},
                { caption: 'Country', text:''},
                { caption: 'Telephone', text:''},
                { caption: 'Email', text:''},
                { caption: 'You are a', text:''},
                { caption: 'If other, please specify', text: '' },
                { caption: 'You are interested in', text: '' },
            ],
            button1Visible: 0,
            formVisible: 1,
    } ,
    methods: {
        getProduct: function () {
            if (window.location.hash) {
                var id = window.location.hash.replace('#', '');
                if (this.products && this.products.length > 0) {
                    for (i in this.products) {
                        if (this.products[i] && this.products[i].id && id == this.products[i].id)
                            this.product = this.products[i];
                    }
                }
            }
        },

        addTotovar: function (id) {
            var tovar = [];

            if (window.localStorage.getItem('tovar')) {
                tovar = window.localStorage.getItem('tovar').split(',');
            }

            if (tovar.indexOf(String(id)) == -1) {
                tovar.push(id);
                window.localStorage.setItem('tovar', tovar.join());
                this.button1Visible = 1;
            }
        },

        checkIntovar: function () {
            if (this.product && this.product.id && window.localStorage.getItem('tovar').split(',').indexOf(String(this.product.id)) != -1) 
                this.button1Visible = 1;
        },

        gettovar: function () {
            var storage = [];
            storage = localStorage.getItem('tovar').split(',')
            for (i in this.products) {
                if (storage.indexOf(String(this.products[i].id)) != -1) {
                    this.tovar.push(this.products[i])
                }
            }
        },
        
        removeFromtovar: function (id) {
            var storage = [];
            storage = window.localStorage.getItem('tovar').split(',')

            storage = storage.filter(storageId => storageId != id)
            window.localStorage.setItem('tovar', storage.join())

            this.tovar = this.tovar.filter(item => item.id != id)
        },

        makeOrder: function () {
            localStorage.clear();
            this.tovar.splice(0, this.tovar.length)
            this.formVisible = 0
        },
        button1Click: function(event) {
            window.open("index4.html");
        }
    },

    mounted: function () {
        this.getProduct();
        this.checkIntovar();
        this.gettovar();
    },

});